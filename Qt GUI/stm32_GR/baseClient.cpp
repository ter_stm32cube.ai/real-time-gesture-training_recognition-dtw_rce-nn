#include "baseClient.h"
#include <QDebug>

#include <signal.h>
#include <unistd.h>

#define AUTH_REQ 1
#define ID_RESP 2

BDClient_Server::BDClient_Server()
{
    ClientPid = -1;
    clientConnection = NULL;
    currentClient = 0;
    clientsList = new QMap<uint16_t,Client>;
    listen(QHostAddress::Any,8080);
    QObject::connect(this, SIGNAL(newConnection()),this, SLOT(ask_connexion()));
}

BDClient_Server::~BDClient_Server()
{

}

pid_t BDClient_Server::getClientPID()
{
    return ClientPid;
}

void BDClient_Server::addClient(Client &c)
{
    clientsList->insert(c.ID,c);
}

void BDClient_Server::rmClient(uint16_t id)
{
    if(clientsList->contains(id))
        clientsList->remove(id);
}

void BDClient_Server::editClient(Client &c)
{
    if(clientsList->contains(c.ID))
        (*clientsList)[c.ID] = c;
}

bool BDClient_Server::authClient(uint16_t &id)
{
    bool ok = false;
    if(clientsList->contains(id))
    {
        if(clientsList->value(id).status > 0)
        {
            ok = true;
        }

    }
    return ok;
}

bool BDClient_Server::doesExist(uint16_t &id)
{
    return clientsList->contains(id);
}

void BDClient_Server::ask_connexion()
{
    //QObject::disconnect(this, SIGNAL(newConnection()), this, SLOT(ask_connexion()));
    //free(clientConnection);
    if(clientConnection)
    {
        kill(ClientPid, SIGINT);
        clientConnection->deleteLater();
    }
    clientConnection = nextPendingConnection();

    QObject::connect(clientConnection, SIGNAL(readyRead()), this, SLOT(readClientPID()));
}

void BDClient_Server::readClient()
{
    char req = 0;
    clientConnection->read(&req, sizeof (req));
    qInfo()<< "REQ = " << (int)req;
    switch (req) {
        case AUTH_REQ: {
            clientConnection->read((char*) &currentClient, sizeof (currentClient));
            qInfo() << "id = " << currentClient;
            bool auth_OK = authClient(currentClient);
            qInfo() << "OK = " << auth_OK;
            clientConnection->write((char*)&auth_OK,sizeof (auth_OK));
            kill(ClientPid,SIGUSR1);
        break;
        }

        case ID_RESP: {
            uint16_t ID = 0;
            clientConnection->read((char*) &ID, sizeof (ID));
            emit newClientID_signal(ID);
        break;
        }
    }
}

void BDClient_Server::readClientPID()
{
    QObject::disconnect(clientConnection, SIGNAL(readyRead()), this, SLOT(readClientPID()));
    QObject::connect(clientConnection, SIGNAL(readyRead()), this, SLOT(readClient()));
    clientConnection->read((char*)&ClientPid, sizeof (ClientPid));
    //ClientPid = (uint8_t)buffer[0] | (((uint16_t)((uint8_t)buffer[1])) << 8);
    qInfo() << "PID = " << ClientPid;
    //qInfo() << (uint8_t) buffer[0] << " "<< (uint8_t)buffer[1] << " "<< (uint8_t)buffer[2] << " "<< (uint8_t)buffer[3] << " " ;
    emit connected_signal();
}
