#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QSerialPort>
#include <QSerialPortInfo>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_pushButton_portsList_clicked();
    void on_pushButton_OCPort_clicked();
    void readData();

    void on_pushButton_newGesture_clicked();

    void on_comboBox_Baude_currentTextChanged(const QString &arg1);

    void on_comboBox_port_currentTextChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;
    QSerialPort *m_serial;
    uint m_nbGestures;

    void RCA(unsigned cmd);
    void RG(unsigned cmd);
    void RG_newGesture();
    void RG_gesture(uint G);
    void settings_reInit();

};
#endif // MAINWINDOW_H
