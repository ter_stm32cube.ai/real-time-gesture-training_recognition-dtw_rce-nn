#ifndef BDCLIENT_SERVER_H
#define BDCLIENT_SERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QObject>

typedef struct
{
    uint16_t ID;
    char lastName[20];
    char firstName[20];
    char Address[52];
    int16_t status;
} Client;

class BDClient_Server : public QTcpServer
{
    Q_OBJECT
public:
    BDClient_Server();
    ~BDClient_Server();
    pid_t getClientPID();
    void addClient(Client &c);
    void rmClient(uint16_t id);
    void editClient(Client &c);
    bool authClient(uint16_t &id);
    bool doesExist(uint16_t &id);

signals:
    void connected_signal();
    void newClientID_signal(uint16_t);

private slots :
    void ask_connexion() ;
    void readClient();
    void readClientPID();

private :
    QTcpSocket *clientConnection;
    char buffer[1024];
    uint16_t ClientPid;
    uint16_t currentClient;
    QMap<uint16_t,Client> *clientsList;
};

#endif // BDCLIENT_SERVER_H
