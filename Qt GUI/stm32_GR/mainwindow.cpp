#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QInputDialog"
#include <QMessageBox>
#include<QDebug>

#define _whatAPP 0x80
#define HWDR_id 0x01
#define GR_id 0x03
#define HWDR_connected 0x70
#define HWDR_gestureExecuted 0x0E
#define HWDR_result 0x0A
#define GR_newGestureRQ 0xF0
#define GR_newGesture 0x08
#define GR_result 0x07

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_serial(new QSerialPort(this))
    , m_nbGestures(0)
{
    ui->setupUi(this);
    m_serial->setDataBits(QSerialPort::Data8);
    m_serial->setParity(QSerialPort::NoParity);
    m_serial->setStopBits(QSerialPort::OneStop);
    m_serial->setFlowControl(QSerialPort::NoFlowControl);
    m_serial->setBaudRate(ui->comboBox_Baude->currentText().toInt());
    QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();
    for (int i = 0;i < ports.length();i++) {
        ui->comboBox_port->addItem(ports.value(i).portName());
    }
    qDebug() << ports.value(0).portName();
    connect(m_serial, &QSerialPort::readyRead, this, &MainWindow::readData);
    ui->Settings->setCurrentIndex(0);
}

MainWindow::~MainWindow()
{
    delete ui;
}




void MainWindow::on_pushButton_portsList_clicked()
{
    QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();
    ui->comboBox_port->clear();
    for (int i = 0;i < ports.length();i++) {
        ui->comboBox_port->addItem(ports.value(i).portName());
    }
}

void MainWindow::on_pushButton_OCPort_clicked()
{
    if(!m_serial->isOpen())
    {
        m_serial->setPortName(ui->comboBox_port->currentText());
        if(m_serial->open(QIODevice::ReadWrite)) {
            ui->pushButton_OCPort->setText("Fermer");
            ui->comboBox_port->setEnabled(0);
            ui->comboBox_Baude->setEnabled(0);
            ui->pushButton_portsList->setEnabled(0);
            QByteArray b(1,(char)_whatAPP);
            m_serial->write(b);

        } else {
            QMessageBox::critical(this, tr("Error"), m_serial->errorString());
        }
    }
    else {
        m_serial->close();
        ui->pushButton_OCPort->setText("Ouvrir");
        ui->comboBox_port->setEnabled(1);
        ui->comboBox_Baude->setEnabled(1);
        ui->pushButton_portsList->setEnabled(1);
        ui->tab_RCA->setEnabled(0);
        ui->tab_RG->setEnabled(0);
        settings_reInit();
    }

}

void MainWindow::readData()
{
    QByteArray m_rData = m_serial->readAll();

    uint8_t x = (uint8_t) m_rData.at(0);
   qDebug() << "data received :" << m_rData.size() << "     " << x << endl;
   switch (x >> 6) {
       case HWDR_id:
           ui->tab_RCA->setEnabled(1);
           ui->tab_RG->setEnabled(0);
           ui->Settings->setCurrentIndex(2);
           RCA(x);
       break;
       case GR_id:
           ui->tab_RCA->setEnabled(0);
           ui->tab_RG->setEnabled(1);
           ui->Settings->setCurrentIndex(3);
           RG(x);
       break;
   }


}

void MainWindow::RCA(unsigned cmd)
{
    if(cmd == HWDR_connected) ui->label_RCA_status->setText("Connecté");
    else if((cmd & 0x0F) == HWDR_gestureExecuted) ui->label_RCA_status->setText("Exécution du geste ...");
    else if((cmd & 0x0F) <= HWDR_result) {
        ui->label_RCA_status->setText("Resultat :");
        ui->lcdNumber->display((int)(cmd & 0x0F));
    }
}

void MainWindow::RG(unsigned cmd)
{
    ui->label_RCA_status->setText("Non connecté");
    ui->lcdNumber->display(15);
    if((cmd & 0x0F) == GR_newGesture) RG_newGesture();
    else if ((cmd & 0x0F) < GR_result) RG_gesture(cmd & 0x0F);

}

void MainWindow::RG_newGesture()
{
    QLabel* L[6] = {ui->label_G1,
                   ui->label_G2,
                   ui->label_G3,
                   ui->label_G4,
                   ui->label_G5,
                   ui->label_G6};
        ui->pushButton_newGesture->setText("Ajouter un geste");
        ui->pushButton_newGesture->setEnabled(1);
        L[m_nbGestures++]->setEnabled(1);
}

void MainWindow::RG_gesture(uint G)
{
    QLabel* L[6] = {ui->label_G1,
                   ui->label_G2,
                   ui->label_G3,
                   ui->label_G4,
                   ui->label_G5,
                   ui->label_G6};

    for(int i = 0; i < 6; i++)
        L[i]->setStyleSheet("");

    if(G)
        if(L[G-1]->isEnabled()) L[G-1]->setStyleSheet("Background-color: #17c558;");
}

void MainWindow::settings_reInit()
{
    QLabel* L[6] = {ui->label_G1,
                   ui->label_G2,
                   ui->label_G3,
                   ui->label_G4,
                   ui->label_G5,
                   ui->label_G6};
    for(int i = 0; i < 6; i++){
        L[i]->setStyleSheet("");
        L[i]->setEnabled(0);
        L[i]->setText(QStringLiteral("Geste %1").arg(i));
        ui->label_RCA_status->setText("Non connecté");
        ui->lcdNumber->display(15);
    }
    ui->pushButton_newGesture->setText("Ajouter un geste");
    ui->pushButton_newGesture->setEnabled(1);
    m_nbGestures = 0;

}


void MainWindow::on_pushButton_newGesture_clicked()
{
    if(m_nbGestures < 6){
        bool ok;
        QLabel* L[6] = {ui->label_G1,
                       ui->label_G2,
                       ui->label_G3,
                       ui->label_G4,
                       ui->label_G5,
                       ui->label_G6};
        QString name = QInputDialog::getText(this, tr("Nouveau geste"), tr("Nom du geste"), QLineEdit::Normal, "Geste", &ok);
        if(ok) {
            L[m_nbGestures]->setText(name);
            ui->pushButton_newGesture->setText("Attente de l'execution du geste ...");
            ui->pushButton_newGesture->setEnabled(0);
            QByteArray b(1,(char)GR_newGestureRQ);
            m_serial->write(b);
        }
    }
}

void MainWindow::on_comboBox_Baude_currentTextChanged(const QString &arg1)
{
    m_serial->setBaudRate(arg1.toInt());
}

void MainWindow::on_comboBox_port_currentTextChanged(const QString &arg1)
{
    m_serial->setPortName(arg1);
}
