/*
 ******************************************************************************
 * @file    main.c
 * @author  Sofiane AOUCI
 * @brief   C Implementation of the DTW/RCE NN for testing
 ******************************************************************************
*/

#include <stdio.h>
#include "data.h"

#define data_size 500
#define network_size 30

double X[data_size] = {3, 2, 2, 1, 4, 3, 4};
double X2[data_size] = {2, 1, 3, 3, 2, 1, 4, 3, 4, 3};
double Y[data_size] = {1, 4, 3, 2, 0, 1, 4};
double Y2[data_size] = {1, 3, 4, 3, 2, 0, 1, 4, 2, 4};

int Xsize = 7;
int Ysize = 6;

double dtw(double* X, double* Y, int n, int m);
double m_min(double* X, double* Y, int *i, int *j);
inline double m_abs(double x);
inline double min(double x, double y, double z);

typedef struct Neuron
{
	double center[data_size];
	double radius;
	unsigned short label; 
} Neuron;

typedef struct network
{
	Neuron N[network_size];
	unsigned number;
} Network;

typedef enum {
	OK,
	full,
	error
} network_return;

network_return train_network(Network *net, double *feature, double R, unsigned short l);
short run_network(Network *net, double *feature);
inline Network new_network();


int main()
{
	Network net = new_network();
	train_network(&net,NS,100,1);
	train_network(&net,NS+500,100,2);
	train_network(&net,NS+1000,100,3);
	train_network(&net,NS+1500,100,4);
	train_network(&net,NS+2000,100,5);
	printf("%lf\n",dtw(NS,NS+20,data_size,data_size));
	printf("net size : %d\nLabel : %d\n",net.number,run_network(&net,NS+1120));
	return 0;
}



double min(double x, double y, double z){
	double m = (x < y) ? x : y;
	return (m < z) ? m : z;
}

double m_abs(double x){
	return (x < 0) ? -x : x;
}

double m_min(double* X, double* Y, int *i, int *j)
{
	int ii = (*i) + 1;
	int jj = (*j);

	if(m_abs(X[ii]-Y[jj]) > m_abs(X[*i]-Y[(*j)+1])){
		ii = (*i);
		jj = (*j) + 1;
	}

	if(m_abs(X[ii]-Y[jj]) >= m_abs(X[(*i)+1]-Y[(*j)+1])){
		ii = (*i) + 1;
		jj = (*j) + 1;
	}
	*i = ii;
	*j = jj;
	printf("%d  %d\n",*i,*j);
	return m_abs(X[ii]-Y[jj]);
}

double dtw(double* X, double* Y, int n, int m)
{
	int i = n, j = 0;
	double *L = X, *C = Y;
	if(n > m){
		L = Y;
		C = X;
		n = m;
		m = i;
	}
	double D[n][2];		
	
	D[0][0] = m_abs(L[0] - C[0]);
	
	for(i = 1; i < n; i++) {
		D[i][0] = m_abs(L[i]-C[0]) + D[i-1][0];
	}

	for(j = 1; j < m; j++){
		D[0][j%2] = D[0][(j+1)%2] + m_abs(L[0]-C[j]);
		
		for(i = 1; i < n; i++){
			D[i][j%2] = m_abs(L[i] - C[j]) + min( D[i-1][j%2], D[i-1][(j+1)%2], D[i][(j+1)%2] );
		}
	}
	return D[n-1][(j+1)%2];
}


Network new_network(){
	Network N;
	N.number = 0;
	return N;
}


network_return train_network(Network *net, double *feature, double R, unsigned short l)
{
	if(net->number == network_size) return full;
	int i, activations = 0, correct_label = -1;
	double D[net->number], min = 9999;
	for (i = 0; i < net->number; ++i)
	{
		D[i] = dtw(feature,net->N[i].center,data_size,data_size);
		if (D[i] <= net->N[i].radius)
		{
			activations++;
			if(net->N[i].label == l) correct_label = i;
			printf("l = %d\n", correct_label);
		}
		else D[i] = 0;
	}

	if (net->number == 0 || activations == 0)
	{
		for (i = 0; i < data_size; ++i)
		{
			net->N[net->number].center[i] = feature[i];
		}
		net->N[net->number].label = l;
		net->N[net->number++].radius = R;
	}

	else if(activations > 1 || correct_label == -1)
	{
		for (i = 0; i < net->number; ++i)
		{
			if (D[i] != 0)
			{
				net->N[i].radius = D[i];
				min = (min > D[i])? D[i] : min;
			}
		}
		if (correct_label < 0)
		{
			for (i = 0; i < data_size; ++i)
			{
				net->N[net->number].center[i] = feature[i];
			}
			net->N[net->number].label = l;
			net->N[net->number++].radius = min;
		}
	}

	return OK;
}

short run_network(Network *net, double *feature)
{
	int i; 
	short l = -1;
	double D, min= 9999;
	for (i = 0; i < net->number; ++i)
	{
		D = dtw(feature,net->N[i].center,data_size,data_size);
		if(D < min){
			min = D;
			l = net->N[i].label;
		}
		
	}
	return l;
}