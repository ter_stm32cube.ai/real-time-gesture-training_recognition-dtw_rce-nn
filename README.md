# AN ACCELEROMETER BASED REAL TIME HAND GESTURE TRAINING AND RECOGNITION APPLICATION ON STM32 MICROCONTROLLER
As part of a project for my master in Embedded Systems and Microsystems, I built this accelerometer based hand gesture recognition application that runs on an STM32 microcontroller with real time gesture training.
It's based on the work published by M. Kim, J. Cho, S. Lee and Y. Jung, in [«IMU Sensor-Based Hand Gesture Recognition for HumanMachine Interfaces»](https://www.youtube.com/redirect?v=_bt6FYoCRfU&redir_token=6vcTwn1-35PgYhGOyh679kiNkQt8MTU4OTcyNTgwMUAxNTg5NjM5NDAx&event=video_description&q=https%3A%2F%2Fwww.mdpi.com%2F1424-8220%2F19%2F18%2F3827) on Sensors, vol. 19, n° %118, p. 3827, on September 4th 2019. 

## Wiki
Go to the [wiki page](https://gitlab.com/ter_stm32cube.ai/real-time-gesture-training_recognition-dtw_rce-nn/-/wikis/AN-ACCELEROMETER-BASED-REAL-TIME-HAND-GESTURE-TRAINING-AND-RECOGNITION-APPLICATION-ON-STM32-MICROCONTROLLER), to learn more about the project.

## Demo Video
[![Demo Video](demo.jpg)](https://youtu.be/_bt6FYoCRfU)