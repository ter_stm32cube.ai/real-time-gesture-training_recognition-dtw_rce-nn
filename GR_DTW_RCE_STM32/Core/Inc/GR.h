/*
 ******************************************************************************
 * @file    GR.h
 * @author  Sofiane AOUCI
 * @brief   Gesture Recognition Application header
 ******************************************************************************
*/
#ifndef INC_GR_H_
#define INC_GR_H_

#include "main.h"
#include "lsm9ds1.h"

#define LSM9DS1_FS LSM9DS1_2g
#define data_size 200
#define network_size 6

typedef lsm9ds1_accel_mg_t coord_t;

typedef struct
{
	coord_t center[data_size];
	uint16_t size;
	float_t radius;
	uint8_t label;
} Neuron;

typedef struct network
{
	Neuron N[network_size];
	uint8_t number;
} Network;

typedef enum {
	OK,
	full,
	error
} network_return;

void GR_Init();
void GR_Calibration();
void GR_data_acquisition();
void GR_data_processing();
network_return GR_add_gesture(coord_t* feature, uint8_t label);
int8_t GR(coord_t *feature);


#endif /* INC_GR_H_ */
