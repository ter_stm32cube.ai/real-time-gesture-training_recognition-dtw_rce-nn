/*
 ******************************************************************************
 * @file    GR.c
 * @author  Sofiane AOUCI
 * @brief   Gesture Recognition Application algorithms
 ******************************************************************************
*/

#include "GR.h"
#include <math.h>

#define max(a,b) a>b?a:b


/*********************************************************************************
 *                              External variables				    			 *
 *********************************************************************************/



/*********************************************************************************
 * 								Private Variables								 *
 *********************************************************************************/
coord_t accel_data[data_size];
coord_t accel_offset;
uint16_t data_length;
uint16_t feature_size;
Network net;

/*********************************************************************************
 *						  Private functions prototypes							 *
 *********************************************************************************/
inline void setValue(coord_t* c, float X, float Y, float Z);
inline void assignValue(coord_t* c, coord_t* d);
inline void equation(coord_t *a, coord_t b, coord_t c, coord_t d);

float_t dtw(coord_t* X, coord_t* Y, uint16_t n, uint16_t m);
//float_t m_min(float_t* X, float_t* Y, int *i, int *j);
inline float_t m_abs(float_t x);
inline float_t min(float_t x, float_t y, float_t z);

network_return train_network(Network *net, coord_t *feature, float_t R, uint8_t l);
int8_t run_network(Network *net, coord_t *feature);
inline Network new_network();



/*********************************************************************************
 * 						  Private functions definition							 *
 *********************************************************************************/

/**
 * assigns X Y Z to the corresponding fields of c
 */
void setValue(coord_t* c, float X, float Y, float Z){
	c->X = X;
	c->Y = Y;
	c->Z = Z;
}

void assignValue(coord_t* c, coord_t* d){
	c->X = d->X;
	c->Y = d->Y;
	c->Z = d->Z;
}

/**
 * It's the implementation of the equation : a = b + c * d
 * It's a way to do additions/multiplications with the coord_t struct
 */
void equation(coord_t *a, coord_t b, coord_t c, coord_t d){
	setValue(a,
			 b.X + c.X * d.X,
			 b.Y + c.Y * d.Y,
			 b.Z + c.Z * d.Z);
}

float_t min(float_t x, float_t y, float_t z){
	float_t m = (x < y) ? x : y;
	return (m < z) ? m : z;
}

float_t m_abs(float_t x){
	return (x < 0) ? -x : x;
}

float_t dtw(coord_t* X, coord_t* Y, uint16_t n, uint16_t m)
{
	int16_t i = n, j = 0;
	coord_t *L = X, *C = Y;
	if(n > m){
		L = Y;
		C = X;
		n = m;
		m = i;
	}
	coord_t D[n][2];

	D[0][0].X = m_abs(L[0].X - C[0].X);
	D[0][0].Y = m_abs(L[0].Y - C[0].Y);
	D[0][0].Z = m_abs(L[0].Z - C[0].Z);

	for(i = 1; i < n; i++) {
		D[i][0].X = m_abs(L[i].X-C[0].X) + D[i-1][0].X;
		D[i][0].Y = m_abs(L[i].Y-C[0].Y) + D[i-1][0].Y;
		D[i][0].Z = m_abs(L[i].Z-C[0].Z) + D[i-1][0].Z;
	}

	for(j = 1; j < m; j++){
		D[0][j%2].X = D[0][(j+1)%2].X + m_abs(L[0].X-C[j].X);
		D[0][j%2].Y = D[0][(j+1)%2].Y + m_abs(L[0].Y-C[j].Y);
		D[0][j%2].Z = D[0][(j+1)%2].Z + m_abs(L[0].Z-C[j].Z);

		for(i = 1; i < n; i++){
			D[i][j%2].X = m_abs(L[i].X - C[j].X) + min( D[i-1][j%2].X, D[i-1][(j+1)%2].X, D[i][(j+1)%2].X );
			D[i][j%2].Y = m_abs(L[i].Y - C[j].Y) + min( D[i-1][j%2].Y, D[i-1][(j+1)%2].Y, D[i][(j+1)%2].Y );
			D[i][j%2].Z = m_abs(L[i].Z - C[j].Z) + min( D[i-1][j%2].Z, D[i-1][(j+1)%2].Z, D[i][(j+1)%2].Z );
		}
	}
	return sqrt((D[n-1][(j+1)%2].X)*(D[n-1][(j+1)%2].X) + (D[n-1][(j+1)%2].Y)*(D[n-1][(j+1)%2].Y) + (D[n-1][(j+1)%2].Z)*(D[n-1][(j+1)%2].Z));
}


Network new_network(){
	Network N;
	N.number = 0;
	return N;
}


network_return train_network(Network *net, coord_t *feature, float_t R, uint8_t l)
{
	if(net->number == network_size) return full;
	int16_t i;
	int8_t activations = 0, correct_label = -1;
	float_t D[net->number], min = 9999;

	for (i = 0; i < net->number; ++i)
	{
		D[i] = dtw(feature,net->N[i].center,feature_size,net->N[i].size);
		if (D[i] <= net->N[i].radius)
		{
			activations++;
			if(net->N[i].label == l) correct_label = i;
			//printf("l = %d\n", correct_label);
		}
		else D[i] = 0;
	}

	if (net->number == 0 || activations == 0)
	{
		for (i = 0; i < data_size; ++i)
		{
			assignValue(net->N[net->number].center + i, feature + i);
		}
		net->N[net->number].label = l;
		net->N[net->number].size = feature_size;
		net->N[net->number++].radius = R;
	}

	else if(activations > 1 || correct_label == -1)
	{
		for (i = 0; i < net->number; ++i)
		{
			if (D[i] != 0)
			{
				net->N[i].radius = D[i];
				min = (min > D[i])? D[i] : min;
			}
		}
		if (correct_label < 0)
		{
			for (i = 0; i < data_size; ++i)
			{
				assignValue(net->N[net->number].center + i, feature + i);
			}
			net->N[net->number].label = l;
			net->N[net->number].size = feature_size;
			net->N[net->number++].radius = min;
		}
	}

	return OK;
}

int8_t run_network(Network *net, coord_t *feature)
{
	int16_t i;
	int8_t l = 0;
	float_t D, min= 9999;
	for (i = 0; i < net->number; ++i)
	{
		D = dtw(feature,net->N[i].center,feature_size,net->N[i].size);
		if(D < min && D < net->N[i].radius){
			min = D;
			l = net->N[i].label;
		}

	}
	return l;
}


/*********************************************************************************
 * 								   Algorithms									 *
 *********************************************************************************/

/**
 * Calibrates the accelerometer
 */
void GR_Calibration()
{
	while (data_length < data_size)
	{
		accel_data[data_length++] = lsm9ds1_from_fsg_to_mg(LSM9DS1_FS, lsm9ds1_readData());
		HAL_Delay(2);
	}

	int i;
	accel_offset.X = 0;
	accel_offset.Y = 0;
	accel_offset.Z = 0;
	for(i = 0; i < data_length; i++){
		accel_offset.X += accel_data[i].X;
		accel_offset.Y += accel_data[i].Y;
		accel_offset.Z += accel_data[i].Z;
	}
	accel_offset.X /= data_length;
	accel_offset.Y /= data_length;
	accel_offset.Z /= data_length;

	data_length = 0;
}

/**
 * Lecture of the accelerometer sensor
 */
void GR_data_acquisition()
{
	accel_data[data_length++] = lsm9ds1_from_fsg_to_mg(LSM9DS1_FS, lsm9ds1_readData());
}

void GR_data_processing()
{
	int i;
	coord_t mean, dummy;
	setValue(&mean, 0, 0, 0);
	setValue(&dummy, -1/data_length, -1/data_length, -1/data_length);
	for(i = 0; i < data_length; i++)
	{
		accel_data[i].Y *= -1;
		mean.X += accel_data[i].X;
		mean.Y += accel_data[i].Y;
		mean.Z += accel_data[i].Z;
	}
	for(i = 0; i < data_length; i++)
	{
		equation(accel_data + i, accel_data[i], mean, dummy);
	}
	for(i = data_length; i < data_size; i++) setValue(accel_data + i, 0, 0, 0);
	feature_size = data_length;
	data_length = 0;
}


network_return GR_add_gesture(coord_t* feature, uint8_t label)
{
	return train_network(&net, feature, 10000, label);
}


int8_t GR(coord_t *feature)
{
	return run_network(&net, feature);
}

void GR_Init()
{
	net = new_network();
	lsm9ds1_Init();
}
