/*
 ******************************************************************************
 * @file    lsm9ds1.c
 * @author  Sofiane AOUCI
 * @brief   LSM9DS1 driver file
 ******************************************************************************
 */

#include "lsm9ds1.h"
#include "main.h"
#include "i2c.h"


void lsm9ds1_Init()
{

	  HAL_GPIO_WritePin(LD2_GPIO_Port,LD1_Pin,1);

	  lsm9ds1_ctrl_reg6_t ctrl_reg6;
	  ctrl_reg6.odr_xl = 6;
	  ctrl_reg6.fs_xl = 0;
	  ctrl_reg6.bw_scal_odr = 1;
	  ctrl_reg6.bw_xl = 3;
	  HAL_I2C_Mem_Write(&hi2c1, LSM9DS1_IMU_I2C_ADD, LSM9DS1_CTRL_REG6, I2C_MEMADD_SIZE_8BIT, (uint8_t*) &ctrl_reg6, sizeof(ctrl_reg6), 1000);


	  lsm9ds1_ctrl_reg7_t ctrl_reg7;
	  ctrl_reg7.hr = 1;
	  ctrl_reg7.dcf = 0;
	  ctrl_reg7.fds = 0;
	  ctrl_reg7.hpis1 = 0;
	  HAL_I2C_Mem_Write(&hi2c1, LSM9DS1_IMU_I2C_ADD, LSM9DS1_CTRL_REG7, I2C_MEMADD_SIZE_8BIT, (uint8_t*) &ctrl_reg7, sizeof(ctrl_reg7), 1000);

}

lsm9ds1_accel_t lsm9ds1_readData()
{
	lsm9ds1_status_reg_t lsm9ds1_status_reg;
	HAL_I2C_Mem_Read(&hi2c1, LSM9DS1_IMU_I2C_ADD, LSM9DS1_STATUS_REG, I2C_MEMADD_SIZE_8BIT, (uint8_t*)&lsm9ds1_status_reg, sizeof(lsm9ds1_status_reg), 1000);

	uint8_t data[6];
	lsm9ds1_accel_t accel;


	if ( lsm9ds1_status_reg.xlda )
	{
		HAL_I2C_Mem_Read(&hi2c1, LSM9DS1_IMU_I2C_ADD, LSM9DS1_OUT_X_L, I2C_MEMADD_SIZE_8BIT, data, 6 * sizeof(uint8_t), 1000);
		accel.X = ((data[1] << 8) | data[0]);
		accel.Y = ((data[3] << 8) | data[2]);
		accel.Z = ((data[5] << 8) | data[4]);
		//HAL_UART_Transmit(&huart3,(uint8_t*) &accel, sizeof(lsm9ds1_accel_t), 1000);
	}

	return accel;

}

lsm9ds1_accel_mg_t lsm9ds1_from_fsg_to_mg(float_t fs, lsm9ds1_accel_t accel)
{
	lsm9ds1_accel_mg_t a;
	a.X = (float_t)accel.X * fs;
	a.Y = (float_t)accel.Y * fs;
	a.Z = (float_t)accel.Z * fs;
	return a;
}


